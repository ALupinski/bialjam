from django.db import models


# Create your models here.


class Directory(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


def directory_path(instance, filename):
    return 'images/{0}/{1}'.format(instance.directory.name, filename)


class Image(models.Model):
    directory = models.ForeignKey(Directory, on_delete=models.CASCADE)
    image = models.ImageField(upload_to=directory_path, blank=True, null=True)
