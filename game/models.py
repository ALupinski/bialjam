# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from award.models import Award
from team.models import Team


def directory_path(instance, filename):
    return 'images/games/{0}'.format(filename)


class Game(models.Model):
    name = models.CharField(max_length=100)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    year = models.PositiveSmallIntegerField()
    url = models.URLField()
    award = models.ForeignKey(Award, on_delete=models.CASCADE, null=True, blank=True)
    description = models.CharField(max_length=250, null=True)
    image = models.ImageField(upload_to=directory_path, blank=True, null=True)

    def __str__(self):
        return self.name
