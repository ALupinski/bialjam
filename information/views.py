# Create your views here.
from django.contrib import messages
from django.template import loader

from .models import Information
from bialjam.core import CustomHttpResponse
from django.utils.translation import ugettext_lazy as _


def view_information(request):
    try:
        info = Information.objects.last()
    except Information.DoesNotExist:
        messages.error(request, _('No information'))
        info = ""
    template = loader.get_template('information/information.html')
    context = {
        'info': info
    }
    return CustomHttpResponse.send(template, context, request)
