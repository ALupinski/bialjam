from django.contrib import messages
from django.shortcuts import render

from django.template import loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Directory, Image
from bialjam.core import CustomHttpResponse
from django.db import transaction
from django.utils.translation import ugettext_lazy as _




def list_directories(request):
    template = loader.get_template('gallery/directories_list.html')

    directory_list = Directory.objects.all()
    paginator = Paginator(directory_list, 6) # Show 6 directories per page
    
    if len(directory_list) == 0:
        messages.error(request, _('No photos to show'))
    else:
        page = request.GET.get('page')
        try:
            directories = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            directories = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            directories = paginator.page(paginator.num_pages)

        context = {
            'directories': directories
        }

        return CustomHttpResponse.send(template, context, request)
    return CustomHttpResponse.send(template, {}, request)


def list_photos(request, id):
    template = loader.get_template('gallery/photos_list.html')

    directory = Directory.objects.get(id = id)

    photo_list = Image.objects.filter( directory = directory)
    paginator = Paginator(photo_list, 6) # Show 6 photos per page

    page = request.GET.get('page')
    try:
        photos = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        photos = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        photos = paginator.page(paginator.num_pages)

    context = {
        'photos': photos
    }

    return CustomHttpResponse.send(template, context, request)
