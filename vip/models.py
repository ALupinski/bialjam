from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
JURY = 'JU'
LECTURER = 'LE'
BOTH = 'BO'


def directory_path(instance, filename):
    return 'images/vips/{0}'.format(filename)


class Vip(models.Model):
    GUEST_CHOICES = (
        (JURY, 'Jury'),
        (LECTURER, 'Lecturer'),
        (BOTH, 'Jury-Lecturer')
    )
    name = models.CharField(max_length=20)
    surname = models.CharField(max_length=50)
    bio = models.CharField(max_length=300)
    guest_as = models.CharField(choices=GUEST_CHOICES, default=JURY, max_length=2)
    image_path = models.ImageField(upload_to=directory_path, blank=True, null=True)

    def __str__(self):
        return self.name + ' ' + self.surname


class Lesson(models.Model):
    date = models.DateTimeField()
    subject = models.CharField(max_length=100)
    lecturer = models.ForeignKey(Vip, on_delete=models.CASCADE, limit_choices_to={'guest_as__in': [LECTURER, BOTH]})

    def clean(self):

        if self.lecturer.guest_as == JURY:
            raise ValidationError('Chosen person is only in jury')
