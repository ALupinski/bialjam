from django.db import models
from datetime import datetime, timedelta, date

class Information(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    description = models.CharField(max_length=2000)
    email = models.EmailField(default='a@b.c')
    contest_date = models.DateField()

    def __str__(self):
        return self.name

    @staticmethod
    def getDaysToContest():
        if Information.objects.count() != 0:
            info = Information.objects.last()
            return (info.contest_date- date.today()).days
        else:
            return 0
