# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models


def directory_path(instance, filename):
    return 'images/teams/{0}'.format(filename)


class Team (models.Model):
    name = models.CharField(max_length=50, unique=True)
    year = models.PositiveSmallIntegerField()
    request_users = models.ManyToManyField(User, through="RequestUserTeam",
                                           through_fields=('team', 'user'))
    logo = models.ImageField(upload_to=directory_path, blank=True, null=True)
    information = models.TextField(max_length=300, blank=True)

    def __str__(self):
        return self.name


class RequestUserTeam(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.CharField(max_length=250)

    def __str__(self):
        return self.team.name + ' - ' + self.user.username
