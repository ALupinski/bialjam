from django.contrib import admin

# Register your models here.
from .models import Game
#
# class InlineAward(admin.TabularInline):
#     model = Award
#
#
# class AwardAdmin(admin.ModelAdmin):
#     inlines = [InlineAward]

admin.site.register(Game)
