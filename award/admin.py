# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Award
#
# class InlineAward(admin.TabularInline):
#     model = Award
#
#
# class AwardAdmin(admin.ModelAdmin):
#     inlines = [InlineAward]

admin.site.register(Award)
