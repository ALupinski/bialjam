from django.template import loader

from .models import Vip, Lesson
from django.utils.translation import ugettext_lazy as _

from bialjam.core import CustomHttpResponse


# Create your views here.
def vip_jury(request):
    template = loader.get_template('vip/vip.html')
    vip_list = Vip.objects.filter(guest_as__in=['JU','LE' ,'BO']).order_by('name')
    context = {'vip_list': vip_list, 'title': _("Jury:")}
    return CustomHttpResponse.send(template, context, request)


# def vip_lect(request):
#     template = loader.get_template('vip/vip.html')
#     vip_list = Vip.objects.filter(guest_as__in=['LE', 'BO'])
#     context = {'vip_list': vip_list, 'title': _("Lecturers:")}
#     return CustomHttpResponse.send(template, context, request)


def vip_details(request, id):
    template = loader.get_template('vip/vip_details.html')

    try:
        vip = Vip.objects.get(id=id)

        lessons = Lesson.objects.filter(lecturer_id=id)

        context = {
            'vip': vip,
            'lessons': lessons
        }

    except Vip.DoesNotExist:
        context = None

    return CustomHttpResponse.send(template, context, request)
